from selenium import webdriver
from itertools import cycle
from PIL import Image

try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk

class App(tk.Tk):
    '''Tk window/label adjusts to size of image'''
    def __init__(self, image_files, x, y, delay):
        # the root will be self
        tk.Tk.__init__(self)

        self.w, self.h = self.winfo_screenwidth(), self.winfo_screenheight()
        print('width={}, height={}'.format(self.w, self.h))
        #self.overrideredirect(1)
        self.focus_set()
        self.bind("<Escape>", lambda e: (e.widget.withdraw(), e.widget.quit()))
        self.geometry('+{}+{}'.format(x, y))

        self.delay = delay

        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(self.w, self.h)

        self.name = 'timeanddate.png'
        self.pageUrl = 'https://www.timeanddate.com/'

        self.image_files = image_files

        try:
            self.driver.get(self.pageUrl)
            self.name = 'timeanddate.png'
            self.driver.save_screenshot(self.name)
        except:
            print("Connection to internet lost")
            self.name = "EricssonNT.png"



        for image in self.image_files:
            img = Image.open(image)
            img = img.resize((self.w, self.h), Image.ANTIALIAS)
            img.save(image, 'png')

        self.pictures = cycle((tk.PhotoImage(file=image), image)
                              for image in self.image_files[0:len(self.image_files)-1])

        self.picture_display = tk.Label(self)
        self.picture_display.pack()
        self.picture_display.configure(background='black')

    def update_image(self):

        try:
            self.driver.get(self.pageUrl)
            self.name = 'timeanddate.png'
            self.driver.save_screenshot(self.name)
        except:
            print("Connection to internet lost")
            self.name="EricssonNT.png"

        #self.img = (cv2.resize(cv2.imread(self.name),\
        #            dsize=(self.w, self.h),\
        #            interpolation=cv2.INTER_CUBIC), self.name)

        self.img = tk.PhotoImage(file=self.name)

    def show_slides(self):
        '''cycle through the images and show them'''
        # next works with Python26 or higher
        img_object, img_name = next(self.pictures)

        if img_name == image_files[0]:
            self.update_image()
        if img_name == image_files[1]:
            self.picture_display.config(image=self.img)
        else:
            self.picture_display.config(image=img_object)

        self.after(self.delay, self.show_slides)


    def run(self):
        self.mainloop()

# set milliseconds time between slides
delay = 6000

# get a series of images you have in the folder
# or use full path, or set directory to where the images are
image_files = [
'1.png',
'timeanddate.png',
'3.png',
'4.png',
'5.png',
'6.png',
'EricssonNT.png'
]

# upper left corner coordinates of app window
x = 0
y = 0

app = App(image_files, x, y, delay)
app.show_slides()
app.run()
